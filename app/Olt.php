<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Olt extends Model
{

    protected $fillable = [
      'olt_index',
      'model',
      'sn',
      'pw'
    ];

}
