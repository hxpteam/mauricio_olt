<?php

namespace App\Http\Controllers;

use App\Olt;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $olt = Olt::where('is_valid', 'false')->orWhere('port', null)->get();
        return view('home', ['olt' => $olt]);
    }

    public function users() {
        $users = User::all();
        return view('users', ['users'=>$users]);
    }

    public function history()
    {
        $olt = Olt::where('port', '!=', null)->get();
        return view('history', ['olt' => $olt]);
    }

    public function olt($id) {
        $olt = Olt::findOrFail($id);
        if (!is_null($olt->port)) return redirect()->route('home');
        return view('olt', ['olt'=>$olt]);
    }

    public function OltSave(Request $request){
        $data = $request->all();

        $olt = Olt::findOrFail($data['id']);

        if (!is_null($olt->port)) return redirect()->route('home');

        $olt->port = $data['port'];
        $olt->is_valid = true;
        $olt->save();
        return redirect()->route('home')->with('status', 'A Porta foi inserida com sucesso!!');
    }

    public function txt() {
        shell_exec('C:\\Python27\\python.exe C:\\py\\job.py');

        sleep(10);

        $fh = fopen('C:\\py\\data.txt','r');

        while ($line = fgets($fh)) {
            if(strpos($line, 'gpon_olt') === false) continue;

            $olt_index = substr($line, 0, 20);
            $model =  substr($line, 20, 21);
            $sn =  substr($line, 41, 19);
            $pw =  substr($line, 60, 20);

            $find = Olt::where([
                'olt_index' => $olt_index,
                'model' => $model,
                'sn' => $sn,
                'pw' => $pw
            ])->count();

            if ($find) continue;

            Olt::create([
                'olt_index' => $olt_index,
                'model' => $model,
                'sn' => $sn,
                'pw' => $pw
            ]);
        }

        fclose($fh);

        return response()->json(['message' => 'ok']);
    }
}
