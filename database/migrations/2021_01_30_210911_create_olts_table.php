<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOltsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('olt_index')->nullable();
            $table->string('model')->nullable();
            $table->string('sn')->nullable();
            $table->string('pw')->nullable();
            $table->string('port')->nullable();
            $table->boolean('is_valid')->nullable()->default(false);
            $table->boolean('is_sync')->nullable()->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olts');
    }
}
