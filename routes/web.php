<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/history', 'HomeController@history')->name('history');
Route::get('/txt', 'HomeController@txt');
Route::get('/olt/{id}', 'HomeController@olt');
Route::post('/olt', 'HomeController@oltSave');
Route::get('/users', 'HomeController@users')->name('users');
