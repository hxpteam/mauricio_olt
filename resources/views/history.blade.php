@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        Historico
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">OltIndex</th>
                                <th scope="col">Model</th>
                                <th scope="col">SN</th>
                                <th scope="col">PW</th>
                                <th scope="col">Porta</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($olt as $key => $o)
                                <tr class="align-middle">
                                    <th scope="row">{{$key + 1}}</th>
                                    <td>{{ $o->olt_index }}</td>
                                    <td>{{ $o->model }}</td>
                                    <td>{{ $o->sn }}</td>
                                    <td>{{ $o->pw }}</td>
                                    <td>{{ $o->port }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                </div>


            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
    <script>
        function executePy() {
            $.get( "/txt", function( data ) {})
            .done(function() {
                location.reload()
            });
        }
    </script>
@endsection
