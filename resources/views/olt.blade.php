@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            Adicionar Porta a OLT
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="/olt" method="POST">
                            @csrf

                            <input type="text" name="id" style="display: none" class="form-control" value="{{$olt->id}}">
                            <div class="form-group">
                                <label for="olt_index">Olt Index</label>
                                <input type="text" id="olt_index" class="form-control" value="{{$olt->olt_index}}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="model">Model</label>
                                <input type="text" id="model" class="form-control" value="{{$olt->model}}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="sn">SN</label>
                                <input type="text" id="sn" class="form-control" value="{{$olt->sn}}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="pw">PW</label>
                                <input type="text" id="pw" class="form-control" value="{{$olt->pw}}" disabled>
                            </div>
                            <div class="form-group">
                                <label for="port">Porta</label>
                                <input type="number" id="port" name="port" class="form-control" value="{{$olt->port}}">
                            </div>
                            <button class="btn btn-primary">Salvar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

